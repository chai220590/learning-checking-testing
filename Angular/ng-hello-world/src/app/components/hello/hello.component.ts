import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-hello',
  template: `<h2>Hello Component</h2>
  <p>Content of Hello Component >> {{ text }}<p>
    <button (click)="OnChildClicked()">Click Child</button>
  `
})

export class HelloComponent implements OnInit {
  @Input() text!: string;
  @Output() buttonClicked: EventEmitter<string> = new EventEmitter<string>();
  // constructor(private _dataService: DataService) {

  // }
  OnChildClicked() {
    this.text = "Changed text by child";
    this.buttonClicked.emit(this.text);

  }
  ngOnInit(): void {
    //this._dataService.setTextFromHello(this.textA);
  }
};
