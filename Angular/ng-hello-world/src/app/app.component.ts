import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template:`<h1 [style.background]="backgroundH1" [style.color]="textcolorH1">Hello-World {{ title }}</h1>

  <h1 [ngStyle]="{color:textcolorH1,background:backgroundH1}">Hello-World {{ title }}</h1>

  <h1 [ngStyle]="styleObj">Hello-World {{ title }}</h1>

  <h1 [class.with-border]="withBorder" [textContent]="title"></h1>
  <button (click)="onClickBtn()">{{withBorder ? "Hide" : "Show"}} border</button>
  <app-hello [text]="title" (buttonClicked)="onButtonClickedFromHello($event)" ></app-hello>
  `,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  backgroundH1 = "black";
  textcolorH1="white";
  title = 'Learning Angular';
  styleObj={
    color:this.textcolorH1,
    background:this.backgroundH1
  };
  withBorder=true;

  onClickBtn(){
    this.withBorder = !this.withBorder;
    this.title = "Clicked Button";
  };

  onButtonClickedFromHello(event:string){
    this.title = event;
  }
}

//Databinding
// 1 propertyBinding
// 2 eventBinding
