import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ng-train-a';
  myString = 'This is my string send to child component';
  childText = "";
  onChildClickedMain(event:string){
    this.myString = event;
  }
}
