import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private _textFrom1Subject : BehaviorSubject<string> = new BehaviorSubject<string>('');
  textFrom1$: Observable<string> = this._textFrom1Subject.asObservable();

  setTextFrom1(text:string){
    this._textFrom1Subject.next(text);
  }
}
