import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { Com01Component } from './components/com01/com01.component';
import { Com02Component } from './components/com02/com02.component';

@NgModule({
  declarations: [
    AppComponent,
    Com01Component,
    Com02Component
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
