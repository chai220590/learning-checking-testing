import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-com01',
  templateUrl: './com01.component.html',
  styleUrls: ['./com01.component.css']
})
export class Com01Component implements OnInit {
  @Input() myString!: string;
  @Output() myClicked : EventEmitter<string> = new EventEmitter<string>();

  objBtn = {color:"blue",background:"tomato",padding:10};

  onClickedChild(){
    this.myClicked.emit('CLICKED FROM CHILD COMPONENT');
    this._dataService.setTextFrom1 ("new text");
  }

  constructor(private _dataService:DataService) {

  }

  ngOnInit(): void {
    this._dataService.setTextFrom1(this.myString);
  }

}
