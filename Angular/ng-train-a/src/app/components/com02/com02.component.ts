import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-com02',
  templateUrl: './com02.component.html',
  styleUrls: ['./com02.component.css']
})
export class Com02Component implements OnInit {
  myString2!: string;
  constructor(private _dataService: DataService) { }

  ngOnInit(): void {
    this._dataService.textFrom1$.subscribe(x=>this.myString2=x);
  }

}
