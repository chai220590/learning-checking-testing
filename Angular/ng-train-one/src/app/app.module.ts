import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { Step1componentComponent } from './components/step1component/step1component.component';
import { Step2componentComponent } from './components/step2component/step2component.component';

@NgModule({
  declarations: [
    AppComponent,
    Step1componentComponent,
    Step2componentComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
