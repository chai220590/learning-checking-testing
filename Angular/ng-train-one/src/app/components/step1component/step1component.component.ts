import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-step1component',
  templateUrl: './step1component.component.html',
  styleUrls: ['./step1component.component.css']
})
export class Step1componentComponent implements OnInit {
  @Input() text!:string;
  @Output() s1Clicked : EventEmitter<string> = new EventEmitter<string>();
  constructor() { }

  ngOnInit(): void {
  }

  onS1Clicked(){
    let s1Text = "Đây là text của S1";
    this.s1Clicked.emit(s1Text);
  }

}
