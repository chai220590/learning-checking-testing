import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Step1componentComponent } from './step1component.component';

describe('Step1componentComponent', () => {
  let component: Step1componentComponent;
  let fixture: ComponentFixture<Step1componentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Step1componentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Step1componentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
