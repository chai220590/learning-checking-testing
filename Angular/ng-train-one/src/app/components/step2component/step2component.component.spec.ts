import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Step2componentComponent } from './step2component.component';

describe('Step2componentComponent', () => {
  let component: Step2componentComponent;
  let fixture: ComponentFixture<Step2componentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Step2componentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Step2componentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
